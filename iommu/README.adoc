// -*- mode: adoc-mode; coding: utf-8; indent-tabs-mode: t; ruler-mode-tab-stops: t; tab-width: 4 -*-

== IOMMU Subsystem testing

These are tests for exercising code paths in the IOMMU and DMA mapping API subsystems
with the system IOMMU in various possible configurations.


=== Directory Structure

The iommu directory contains the following:

boot:: - Original boot test for iommu.
include:: - Helper code for implementing involving iommus.
new-boot:: - New test for testing booting system with iommu in various configurations.

.Directory Structure
----
iommu
├── boot
├── include
└── new-boot
----


=== Current Implementation Status

==== Done

include:: - Helper code for running new tests involving an iommu 
new-boot:: - New boot test 

==== In progress

io/fio:: - Test for running fio jobs against system with iommu in various configurations.
io/stress-ng:: - Test for running iomix, or arbitrary stressor jobs in stress-ng against various iommu configurations.

==== Potential Future work

crash:: - Test for verifying successful crashkernel boot, and vmcore harvest on systems with iommu in various configurations.
perf:: - Tests for measuring i/o performance on systems with the iommu in various configurations.


'''


=== Test Parameters

new-boot: ::
	DMA_MODNAME: :::
		*amd_iommu* | *intel_iommu* | *arm_smmu*
	DMA_IOMMU_CONF: :::
		common to all iommus: ::::
			   *lazy* | *strict* | *pt* | *forcdac*
		
		intel_iommu: ::::
			*sm-off-lazy* | *sm-off-strict* | *sm-off-pt* | *sm-on-lazy* | *sm-on-strict* | *sm-on-pt* | *sp_off* | *igfx_off* : _sm denotes Scalable Mode, which is a technology on SPR and later systems_
		amd_iommu: ::::
			*pgtbl_v2-lazy* | *pgtbl_v2-strict* | *pgtbl_v2-pt*


'''


=== Host Requirements

intel_iommu: ::
		key_value: :::
			op: "=" +
			name: "*VIRT_IOMMU*" +
			value: "*1*" +
		cpu: :::
			vendor: ::::
				op: "like" +
				value: "*%Intel*"
		system: :::
			arch: ::::
				op: "=" +
				value: "*x86_64*"

intel_iommu with Scalable Mode support: ::
		key_value: :::
			op: "=" +
			name: "*VIRT_IOMMU*" +
			value: "*1*" +
		cpu: :::
			vendor: ::::
				op: "like" +
				value: "*%Intel*"
			flag: ::::
				op: "=" +
				value: "movdir64b"
			flag: ::::
				op: "=" +
				value: "enqcmd"
		system: :::
			arch: ::::
				op: "=" +
				value: "*x86_64*"

System with AMD IOMMU: ::
		key_value: :::
			op: "=" +
			name: "*VIRT_IOMMU*" +
			value: "*1*" +
		cpu: :::
			vendor: ::::
				op: "like" +
				value: "*%AMD*"
		system: :::
			arch: ::::
				op: "=" +
				value: "*x86_64*"

System with ARM SMMU: ::
		key_value: :::
			op: "=" +
			name: "*VIRT_IOMMU*" +
			value: "*1*" +
		system: :::
			arch: ::::
				op: "=" +
				value: "*aarch64*"
